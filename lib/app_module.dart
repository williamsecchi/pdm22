import 'package:flutter_modular/flutter_modular.dart';
import 'package:pdm22/core/interfaces/http_interface.dart';
import 'package:pdm22/core/service/http_service.dart';
import 'package:pdm22/modules/home/home_module.dart';

import 'modules/splash_module/splash_module.dart';
import 'package:modular_interfaces/modular_interfaces.dart';

class AppModule extends Module {
  @override
  List<Bind> get binds => <Bind<Object>>[
        Bind<HttpInterface>(
          (Injector<dynamic> i) => HttpService(),
        ),
      ];

  @override
  List<ModularRoute> get routes => <ModularRoute>[
        ModuleRoute<ModularRoute>(
          '/',
          module: SplashModule(),
        ),
        ModuleRoute<ModularRoute>(
          '/home/',
          module: HomeModule(),
          transition: TransitionType.fadeIn,
        ),
      ];
}
