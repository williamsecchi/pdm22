abstract class HttpInterface {
  Future<Map<String, dynamic>> get({
    required String queryString,
  });

  Future<Map<String, dynamic>> post({
    required String queryString,
    required Map<String, dynamic> body,
  });

  Future<Map<String, dynamic>> put({
    required String queryString,
    required Map<String, dynamic> body,
  });

  Future<Map<String, dynamic>> delete(dynamic T);
}