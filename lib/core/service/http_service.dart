import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';
import 'package:http/io_client.dart';
import 'package:pdm22/core/interfaces/http_interface.dart';

class HttpService implements HttpInterface {
  static const Duration duration = Duration(seconds: 10);

  @override
  Future<Map<String, dynamic>> delete(T) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<Map<String, dynamic>> get({
    required String queryString,
  }) async {
    try {
      final HttpClient ioc = HttpClient();
      ioc.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      final IOClient http = IOClient(ioc);

      final Response response = await http.get(
        Uri.parse(queryString),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
      ).timeout(duration);

      String returnValue = utf8.decode(response.bodyBytes);

      if (returnValue.startsWith('[')) {
        returnValue = '{"data": $returnValue}';
      }

      try {
        return jsonDecode(returnValue) as Map<String, dynamic>;
      } catch (e) {
        return <String, dynamic>{'value': jsonDecode(returnValue)};
      }
    } on TimeoutException catch (e) {
      return <String, String>{'connection': e.toString()};
    } catch (e) {
      return <String, String>{'error': e.toString()};
    }
  }

  @override
  Future<Map<String, dynamic>> post({
    required String queryString,
    required Map<String, dynamic> body,
  }) {
    // TODO: implement post
    throw UnimplementedError();
  }

  @override
  Future<Map<String, dynamic>> put({
    required String queryString,
    required Map<String, dynamic> body,
  }) {
    // TODO: implement put
    throw UnimplementedError();
  }
}
