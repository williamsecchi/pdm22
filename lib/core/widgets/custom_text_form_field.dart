import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

class CustomTextFormField extends StatefulWidget {
  const CustomTextFormField({
    Key? key,
    required this.label,
    required this.hint,
    required this.controller,
    required this.validator,
    this.password = false,
    this.keyboardType = TextInputType.text,
    this.textInputAction = TextInputAction.next,
    this.inputFormatters = const <TextInputFormatter>[],
    this.maxlines = 1,
    this.onChanged,
  }) : super(key: key);

  final String label;
  final String hint;
  final bool password;
  final TextEditingController controller;
  final FormFieldValidator<String> validator;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final List<TextInputFormatter> inputFormatters;
  final int maxlines;
  final ValueChanged<String>? onChanged;

  @override
  State<CustomTextFormField> createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  bool _passwordVisible = false;

  @override
  void initState() {
    _passwordVisible = widget.password;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(
          color: Colors.blue,
          width: 0.8,
        ),
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: const <BoxShadow>[
          BoxShadow(
            color: Colors.white,
            blurRadius: 3.0,
            offset: Offset(0.0, 0.5),
          )
        ],
      ),
      height: 50,
      width: size.width * 0.8,
      child: TextFormField(
        textCapitalization: !widget.password
            ? widget.keyboardType != TextInputType.emailAddress
            ? TextCapitalization.words
            : TextCapitalization.none
            : TextCapitalization.none,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        inputFormatters: widget.inputFormatters,
        controller: widget.controller,
        obscureText: _passwordVisible,
        validator: widget.validator,
        keyboardType: widget.keyboardType,
        textInputAction: widget.textInputAction,
        maxLines: widget.maxlines,
        cursorColor: Colors.black,
        textAlign: TextAlign.left,
        onChanged: widget.onChanged,
        style: const TextStyle(
          fontSize: 14,
          color: Colors.black,
          height: 1.5,
        ),
        decoration: InputDecoration(
          suffixIcon: !widget.password
              ? const SizedBox(
            height: 0.0,
            width: 0.0,
          )
              : IconButton(
            onPressed: () {
              setState(() {
                _passwordVisible = !_passwordVisible;
              });
            },
            icon: _passwordVisible
                ? const Icon(
              Icons.visibility,
              color: Colors.blue,
            )
                : const Icon(Icons.visibility_off),
          ),
          contentPadding: const EdgeInsets.only(
            left: 15.0,
            right: 15.0,
          ),
          labelText: widget.label,
          hintText: widget.hint,
          border: const OutlineInputBorder(borderSide: BorderSide.none),
          errorStyle: const TextStyle(
            fontSize: 10,
            height: 0.5,
          ),
          labelStyle: const TextStyle(
            height: 1.5,
          ),
          errorBorder: InputBorder.none,
          floatingLabelStyle: const TextStyle(
            color: Colors.blue,
            height: 0.8,
          ),
          floatingLabelBehavior: FloatingLabelBehavior.never,
        ),
      ),
    );
  }
}
