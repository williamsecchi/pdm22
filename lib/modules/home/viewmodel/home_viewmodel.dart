import 'package:flutter/material.dart';

class HomeViewModel {
  void navigate(BuildContext context, String route) {
    Navigator.pushNamed(context, route);
  }
}
