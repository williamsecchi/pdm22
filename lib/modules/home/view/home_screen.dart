import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pdm22/modules/home/viewmodel/home_viewmodel.dart';

import '../../../core/widgets/custom_buttom.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final HomeViewModel instance = Modular.get<HomeViewModel>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  AppBar _appBar() {
    return AppBar(
      title: const Text('Exercícios Propostos'),
    );
  }

  SizedBox _body() {
    return SizedBox.expand(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CustomButtom(
            context: context,
            name: 'Exer 1',
            function: () =>
                instance.navigate(context, '${Modular.to.path}exer1'),
          ),
          CustomButtom(
            context: context,
            name: 'Exer 2',
            function: () =>
                instance.navigate(context, '${Modular.to.path}exer2'),
          ),
        ],
      ),
    );
  }
}
