import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pdm22/modules/exer1/exer1_module.dart';
import 'package:pdm22/modules/exer2/exer2_module.dart';
import 'package:pdm22/modules/home/view/home_screen.dart';
import 'package:pdm22/modules/home/viewmodel/home_viewmodel.dart';
import 'package:modular_interfaces/modular_interfaces.dart';

class HomeModule extends Module {
  @override
  List<Bind> get binds => <Bind<Object>>[
        Bind<HomeViewModel>(
          (Injector<dynamic> i) => HomeViewModel(),
        ),
      ];

  @override
  List<ModularRoute> get routes => <ModularRoute>[
        ChildRoute<ModularRoute>(
          '/',
          child: (BuildContext context, ModularArguments args) {
            return const HomeScreen();
          },
        ),
        ModuleRoute<Exer1Module>(
          '/exer1/',
          module: Exer1Module(),
          transition: TransitionType.rightToLeft,
        ),
        ModuleRoute<ModularRoute>(
          'exer2',
          module: Exer2Module(),
          transition: TransitionType.rightToLeft,
        ),
      ];
}
