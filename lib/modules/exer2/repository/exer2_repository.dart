import 'package:pdm22/core/interfaces/http_interface.dart';
import 'package:pdm22/modules/exer2/interfaces/repository_interface.dart';

class Exer2Repository implements IRepository {
  HttpInterface http;

  Exer2Repository({
    required this.http,
  });

  @override
  Future<Map<String, dynamic>> buscaCep(String cep) async {
    final String url = 'https://viacep.com.br/ws/$cep/json/';

    Map<String, dynamic> response = await http.get(
      queryString: url,
    );

    if (response.containsKey('erro') || response.containsKey('connection')) {
      return <String, dynamic>{'erro': 'CEP não encontrado'};
    }

    return response;
  }

  @override
  Future<Map<String, dynamic>> getClima() async {
    const String url =
        'https://weather.contrateumdev.com.br/api/weather/city/?city=Erechim,rio%20grande%20do%20sul';
    ;

    Map<String, dynamic> response = await http.get(
      queryString: url,
    );

    if (response.containsKey('erro') || response.containsKey('connection')) {
      return <String, dynamic>{'erro': 'Informações do clima não encontradas'};
    }

    return response;
  }

  @override
  Future<Map<String, dynamic>> getBitcoin() async {
    const String url = 'https://www.mercadobitcoin.net/api/BTC/ticker/';
    Map<String, dynamic> response = await http.get(
      queryString: url,
    );

    if (response.containsKey('erro') || response.containsKey('connection')) {
      return <String, dynamic>{
        'erro': 'Informações do Bitcoin não encontradas'
      };
    }

    return response;
  }
}
