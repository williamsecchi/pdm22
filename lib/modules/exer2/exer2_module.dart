import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pdm22/modules/exer2/repository/exer2_repository.dart';
import 'package:pdm22/modules/exer2/view/bitcoin_screen.dart';
import 'package:pdm22/modules/exer2/view/cep_screen.dart';
import 'package:pdm22/modules/exer2/view/clima_screen.dart';
import 'package:pdm22/modules/exer2/view/exer2_screen.dart';
import 'package:pdm22/modules/exer2/viewmodel/exer2_viewmodel.dart';
import 'package:modular_interfaces/modular_interfaces.dart';

class Exer2Module extends Module {
  @override
  List<Bind> get binds => <Bind<Object>>[
        Bind<Exer2ViewModel>(
          (Injector<dynamic> i) => Exer2ViewModel(
            iRepository: i.get(),
          ),
        ),
        Bind<Exer2Repository>(
          (Injector<dynamic> i) => Exer2Repository(
            http: i.get(),
          ),
        ),
      ];

  @override
  List<ModularRoute> get routes => <ModularRoute>[
        ChildRoute<ModularRoute>(
          '/',
          child: (BuildContext context, ModularArguments args) {
            return const Exer2Screen();
          },
        ),
        ChildRoute<ModularRoute>(
          '/cep/',
          child: (BuildContext context, ModularArguments args) {
            return const CepScreen();
          },
          transition: TransitionType.fadeIn,
        ),
        ChildRoute<ModularRoute>(
          '/clima/',
          child: (BuildContext context, ModularArguments args) {
            return const ClimaScreen();
          },
          transition: TransitionType.fadeIn,
        ),
        ChildRoute<ModularRoute>(
          '/bitcoin/',
          child: (BuildContext context, ModularArguments args) {
            return const BitcoinScreen();
          },
          transition: TransitionType.fadeIn,
        ),
      ];
}
