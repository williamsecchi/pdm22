import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pdm22/modules/exer2/model/exer2_model.dart';
import 'package:pdm22/modules/exer2/model/via_cep.dart';
import '../interfaces/repository_interface.dart';
import '../model/bitcoin.dart';
import '../model/clima.dart';

class Exer2ViewModel {
  IRepository iRepository;

  Exer2ViewModel({
    required this.iRepository,
  });

  final Exer2Model exer2model = Exer2Model();

  void navigate(BuildContext context, String route) {
    Navigator.pushNamed(context, route);
  }

  void disposeCep() {
    exer2model.viaCepController.close();
  }

  void initCep() {
    exer2model.viaCepController = StreamController<ViaCep>();
    exer2model.cepController.clear();
  }

  void buscaCep(String cep) async {
    final Map<String, dynamic> map = await iRepository.buscaCep(cep);

    if (!map.containsKey('erro')) {
      exer2model.viaCepController.sink.add(ViaCep.fromJson(map));
    } else {
      exer2model.viaCepController.sink.addError(map['erro']);
    }
  }

  Future<Clima> getClima() async {
    final Map<String, dynamic> map = await iRepository.getClima();

    if (!map.containsKey('erro')) {
      return Clima.fromJson(map);
    } else {
      throw map['erro'];
    }
  }

  Future<Bitcoin> getBitcoin() async {
    final Map<String, dynamic> map = await iRepository.getBitcoin();

    if (!map.containsKey('erro')) {
      return Bitcoin.fromJson(map);
    } else {
      throw map['erro'];
    }
  }
}
