import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pdm22/core/widgets/custom_text_form_field.dart';
import 'package:pdm22/modules/exer2/model/via_cep.dart';
import 'package:pdm22/modules/exer2/viewmodel/exer2_viewmodel.dart';

class CepScreen extends StatefulWidget {
  const CepScreen({Key? key}) : super(key: key);

  @override
  State<CepScreen> createState() => _CepScreenState();
}

class _CepScreenState extends State<CepScreen> {
  final Exer2ViewModel viewModel = Modular.get<Exer2ViewModel>();

  @override
  void initState() {
    super.initState();
    viewModel.initCep();
  }

  @override
  void dispose() {
    viewModel.disposeCep();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  AppBar _appBar() {
    return AppBar(
      title: const Text('BUSCA CEP'),
    );
  }

  Container _body() {
    return Container(
      padding: const EdgeInsets.all(16.0),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Hero(
            tag: 'cep',
            child: Material(
              child: CustomTextFormField(
                label: 'Cep',
                hint: 'Digite um Cep',
                controller: viewModel.exer2model.cepController,
                validator: (value) => null,
                inputFormatters: [
                  FilteringTextInputFormatter.digitsOnly,
                  CepInputFormatter(),
                ],
                keyboardType: TextInputType.number,
                onChanged: (String? value) {
                  if (value != null && value.length == 10) {
                    viewModel.buscaCep(
                      value.replaceAll('-', '').replaceAll('.', ''),
                    );
                  }
                },
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 16.0),
            child: StreamBuilder<ViaCep>(
              stream: viewModel.exer2model.viaCepController.stream,
              builder: (BuildContext context, AsyncSnapshot<ViaCep> snapshot) {
                if (snapshot.hasData) {
                  return Text(
                    'CEP: ${snapshot.data?.cep} - ${snapshot.data?.logradouro} - ${snapshot.data?.bairro} - ${snapshot.data?.localidade} - ${snapshot.data?.uf}',
                  );
                }
                if (snapshot.hasError) {
                  return Text(snapshot.error.toString());
                }
                return Container();
              },
            ),
          ),
        ],
      ),
    );
  }
}
