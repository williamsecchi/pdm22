import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../model/clima.dart';
import '../viewmodel/exer2_viewmodel.dart';

class ClimaScreen extends StatefulWidget {
  const ClimaScreen({Key? key}) : super(key: key);

  @override
  State<ClimaScreen> createState() => _ClimaScreenState();
}

class _ClimaScreenState extends State<ClimaScreen> {
  final Exer2ViewModel viewModel = Modular.get<Exer2ViewModel>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  AppBar _appBar() {
    return AppBar(
      title: const Text('CLIMA'),
    );
  }

  SingleChildScrollView _body() {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(16.0),
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            const Hero(
              tag: 'clima',
              child: Material(
                child: Text('Tempo em Erechim'),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(16.0),
              child: FutureBuilder<Clima>(
                future: viewModel.getClima(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const CircularProgressIndicator();
                  } else {
                    if (snapshot.hasError) {
                      return Text('Erro: ${snapshot.error}');
                    } else {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text(
                            'Temperatura: ${snapshot.data!.main?.temp ?? 'Não foi possível obter a temperatura!'}',
                          ),
                          Text(
                            'Temperatura Máxima: ${snapshot.data!.main?.tempMax ?? 'Não foi possível obter a temperatura máxima!'}',
                          ),
                          Text(
                            'Temperatura Mínima: ${snapshot.data!.main?.tempMin ?? 'Não foi possível obter a temperatura mínima!'}',
                          ),
                          Text(
                            'Pressão: ${snapshot.data!.main?.pressure ?? 'Não foi possível obter a pressão!'}',
                          ),
                          Text(
                            'Umidade: ${snapshot.data!.main?.humidity ?? 'Não foi possível obter a umidade!'}',
                          ),
                          Text(
                            'Velocidade do Vento: ${snapshot.data!.wind?.speed ?? 'Não foi possível obter a velocidade do vento!'}',
                          ),
                          Text(
                            'Direção do Vento: ${snapshot.data!.wind?.deg ?? 'Não foi possível obter a direção do vento!'}',
                          ),
                          Text(
                            'Latitude: ${snapshot.data!.coord?.lat ?? 'Não foi possível obter a latitude!'}',
                          ),
                          Text(
                            'Longitude: ${snapshot.data!.coord?.lon ?? 'Não foi possível obter a longitude!'}',
                          ),
                          Text(
                            'Cidade: ${snapshot.data!.name ?? 'Não foi possível obter a cidade!'}',
                          ),
                          Text(
                            'País: ${snapshot.data!.sys?.country ?? 'Não foi possível obter o país!'}',
                          ),
                          Text(
                            'Descrição: ${snapshot.data!.weather?.first.description ?? 'Não foi possível obter a descrição!'}',
                          ),
                        ],
                      );
                    }
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
