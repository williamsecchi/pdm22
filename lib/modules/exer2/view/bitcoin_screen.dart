import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pdm22/modules/exer2/model/bitcoin.dart';
import 'package:pdm22/modules/exer2/viewmodel/exer2_viewmodel.dart';

class BitcoinScreen extends StatefulWidget {
  const BitcoinScreen({Key? key}) : super(key: key);

  @override
  State<BitcoinScreen> createState() => _BitcoinScreenState();
}

class _BitcoinScreenState extends State<BitcoinScreen> {
  final Exer2ViewModel viewModel = Modular.get<Exer2ViewModel>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  AppBar _appBar() {
    return AppBar(
      title: const Text('BITCOIN'),
    );
  }

  SingleChildScrollView _body() {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(16.0),
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            const Hero(
              tag: 'bitcoin',
              child: Material(
                child: Text('Informações sobre Bitcoin'),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(16.0),
              child: FutureBuilder<Bitcoin>(
                future: viewModel.getBitcoin(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const CircularProgressIndicator();
                  } else {
                    if (snapshot.hasError) {
                      return Text('Erro: ${snapshot.error}');
                    } else {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text(
                            'Maior preço unitário de negociação das últimas 24 horas: ${snapshot.data!.ticker?.high ?? 'N/A'}',
                          ),
                          Text(
                            'Menor preço unitário de negociação das últimas 24 horas: ${snapshot.data!.ticker?.low ?? 'N/A'}',
                          ),
                          Text(
                            'Quantidade negociada nas últimas 24 horas: ${snapshot.data!.ticker?.vol ?? 'N/A'}',
                          ),
                          Text(
                            'Preço unitário da última negociação: ${snapshot.data!.ticker?.last ?? 'N/A'}',
                          ),
                          Text(
                            'Maior preço de oferta de compra das últimas 24 horas: ${snapshot.data!.ticker?.buy ?? 'N/A'}',
                          ),
                          Text(
                            'Menor preço de oferta de venda das últimas 24 horas: ${snapshot.data!.ticker?.sell ?? 'N/A'}',
                          ),
                        ],
                      );
                    }
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
