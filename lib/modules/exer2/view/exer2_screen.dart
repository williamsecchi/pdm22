import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pdm22/core/widgets/custom_buttom.dart';
import 'package:pdm22/modules/exer2/viewmodel/exer2_viewmodel.dart';

class Exer2Screen extends StatefulWidget {
  const Exer2Screen({Key? key}) : super(key: key);

  @override
  State<Exer2Screen> createState() => _Exer2ScreenState();
}

class _Exer2ScreenState extends State<Exer2Screen> {
  final Exer2ViewModel viewModel = Modular.get<Exer2ViewModel>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  AppBar _appBar() {
    return AppBar(
      title: const Text('Exer 2'),
    );
  }

  SizedBox _body() {
    return SizedBox.expand(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Hero(
            tag: 'cep',
            child: CustomButtom(
              context: context,
              name: 'CEP',
              function: () => viewModel.navigate(context, 'cep/'),
            ),
          ),
          Hero(
            tag: 'clima',
            child: CustomButtom(
              context: context,
              name: 'CLIMA',
              function: () => viewModel.navigate(context, 'clima/'),
            ),
          ),
          Hero(
            tag: 'bitcoin',
            child: CustomButtom(
              context: context,
              name: 'BITCOIN',
              function: () => viewModel.navigate(context, 'bitcoin/'),
            ),
          ),
        ],
      ),
    );
  }
}
