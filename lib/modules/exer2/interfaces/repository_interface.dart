abstract class IRepository {
  Future<Map<String, dynamic>> buscaCep(String cep);
  Future<Map<String, dynamic>> getClima();
  Future<Map<String, dynamic>> getBitcoin();
}
