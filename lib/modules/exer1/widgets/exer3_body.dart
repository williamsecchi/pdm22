import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdm22/core/widgets/custom_text_form_field.dart';

class Exer3Body extends StatefulWidget {
  const Exer3Body({Key? key}) : super(key: key);

  @override
  State<Exer3Body> createState() => _Exer3BodyState();
}

class _Exer3BodyState extends State<Exer3Body> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _controller = TextEditingController();

  ValueNotifier<bool> showValue = ValueNotifier<bool>(false);

  late int valueToShow;

  @override
  void initState() {
    super.initState();
    _formKey.currentState?.reset();
    showValue.value = false;
    _controller.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          CustomTextFormField(
            label: 'Valor',
            hint: 'Digite um valor positivo',
            controller: _controller,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 20,
          ),
          ValueListenableBuilder(
            valueListenable: showValue,
            builder: (_, bool value, Widget? child) {
              if (value) {
                return Text('O quadrado é: $valueToShow');
              }
              return Container();
            },
          )
        ],
      ),
    );
  }

  onChanged(String? value) {
    if (!_formKey.currentState!.validate()) return;
    setState(() {
      showValue.value = true;
      int auxNum = int.parse(_controller.text.trim());
      int auxQuadrado = 0;
      for (int i = 0; i < auxNum; i++) {
        auxQuadrado += auxNum;
      }
      valueToShow = auxQuadrado;
    });
  }

  String? validator(String? value) {
    if (value!.isEmpty) {
      showValue.value = false;
      return 'Digite um número positivo.';
    }

    return null;
  }
}
