import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdm22/core/widgets/custom_text_form_field.dart';

class Exer13Body extends StatefulWidget {
  const Exer13Body({Key? key}) : super(key: key);

  @override
  State<Exer13Body> createState() => _Exer13BodyState();
}

class _Exer13BodyState extends State<Exer13Body> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _controllerAtual = TextEditingController();

  final TextEditingController _controllerAnterior = TextEditingController();

  final TextEditingController _controllerLitros = TextEditingController();

  ValueNotifier<bool> showValue = ValueNotifier<bool>(false);

  ValueNotifier<bool> showError = ValueNotifier<bool>(false);

  late String valueToShow;

  @override
  void initState() {
    super.initState();
    _formKey.currentState?.reset();
    showValue.value = false;
    _controllerAtual.clear();
    _controllerAnterior.clear();
    _controllerLitros.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          CustomTextFormField(
            label: 'Atual',
            hint: 'Quilometragem atual',
            controller: _controllerAtual,
            validator: validator,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 10,
          ),
          CustomTextFormField(
            label: 'Anterior',
            hint: 'Quilometragem anterior',
            controller: _controllerAnterior,
            validator: validator,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 10,
          ),
          CustomTextFormField(
            label: 'Litros',
            hint: 'Qnt litros consumidos',
            controller: _controllerLitros,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 20,
          ),
          ValueListenableBuilder(
            valueListenable: showValue,
            builder: (_, bool value, Widget? child) {
              if (value) {
                return Text('TX Consumo: $valueToShow litro(s) por KM');
              }
              return Container();
            },
          ),
          ValueListenableBuilder(
            valueListenable: showError,
            builder: (_, bool value, Widget? child) {
              if (value) {
                return const Text('Quilometragem atual não pode ser menor que a quilometragem anterior');
              }
              return Container();
            },
          )
        ],
      ),
    );
  }

  onChanged(String? value) {
    if (!_formKey.currentState!.validate()) return;
    setState(() {
      showValue.value = true;
      showError.value = false;
      valueToShow = results() ?? '';
    });
  }

  String? results() {
    int kmAtual = int.parse(_controllerAtual.text.trim());
    int kmAnterior = int.parse(_controllerAnterior.text.trim());
    int litrosConsumidos = int.parse(_controllerLitros.text.trim());

    if (kmAnterior > kmAtual) {
      showValue.value = false;
      showError.value = true;
      return null;
    }

    int kms = kmAtual - kmAnterior;

    return (kms / litrosConsumidos).toStringAsFixed(2);
  }
  String? validator(String? value) {
    if (value!.isEmpty) {
      showValue.value = false;
      return 'Digite um número positivo.';
    }

    return null;
  }
}
