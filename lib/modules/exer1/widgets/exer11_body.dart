import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdm22/core/widgets/custom_text_form_field.dart';

class Exer11Body extends StatefulWidget {
  const Exer11Body({Key? key}) : super(key: key);

  @override
  State<Exer11Body> createState() => _Exer11BodyState();
}

class _Exer11BodyState extends State<Exer11Body> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _controller = TextEditingController();

  ValueNotifier<bool> showValue = ValueNotifier<bool>(false);

  late double valueToShow;

  @override
  void initState() {
    super.initState();
    _formKey.currentState?.reset();
    showValue.value = false;
    _controller.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          CustomTextFormField(
            label: 'Minutos',
            hint: 'Digite o valor',
            controller: _controller,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 20,
          ),
          ValueListenableBuilder(
            valueListenable: showValue,
            builder: (_, bool value, Widget? child) {
              if (value) {
                return Text(
                    'O valor será: R\$ ${valueToShow.toStringAsFixed(2)}');
              }
              return Container();
            },
          ),
        ],
      ),
    );
  }

  onChanged(String? value) {
    if (!_formKey.currentState!.validate()) return;
    setState(() {
      showValue.value = true;
      valueToShow = results();
    });
  }

  double results() {
    if (int.parse(_controller.text.trim()) <= 3) {
      return 1.15;
    }
    int duration = int.parse(_controller.text.trim());
    int auxValueToCharge = duration - 3;
    double additional = auxValueToCharge * 0.26;
    return 1.15 + additional;
  }

  String? validator(String? value) {
    if (value!.isEmpty) {
      showValue.value = false;
      return 'Digite um número positivo.';
    }

    return null;
  }
}
