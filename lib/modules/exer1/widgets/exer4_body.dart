import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdm22/core/widgets/custom_text_form_field.dart';

import '../../../core/widgets/decimal_input_formatter.dart';

class Exer4Body extends StatefulWidget {
  const Exer4Body({Key? key}) : super(key: key);

  @override
  State<Exer4Body> createState() => _Exer4BodyState();
}

class _Exer4BodyState extends State<Exer4Body> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _controller1 = TextEditingController();

  final TextEditingController _controller2 = TextEditingController();

  final TextEditingController _controller3 = TextEditingController();

  ValueNotifier<bool> showValue = ValueNotifier<bool>(false);

  late double valueToShow;

  @override
  void initState() {
    super.initState();
    _formKey.currentState?.reset();
    showValue.value = false;
    _controller1.clear();
    _controller2.clear();
    _controller3.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          CustomTextFormField(
            label: 'Valor 1',
            hint: 'Digite um valor',
            controller: _controller1,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: const TextInputType.numberWithOptions(decimal: true),
            inputFormatters: [
              DecimalTextInputFormatter(decimalRange: 2),
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 10,
          ),
          CustomTextFormField(
            label: 'Valor 2',
            hint: 'Digite um valor',
            controller: _controller2,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: const TextInputType.numberWithOptions(decimal: true),
            inputFormatters: [
              DecimalTextInputFormatter(decimalRange: 2),
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 10,
          ),
          CustomTextFormField(
            label: 'Valor 3',
            hint: 'Digite um valor',
            controller: _controller3,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: const TextInputType.numberWithOptions(decimal: true),
            inputFormatters: [
              DecimalTextInputFormatter(decimalRange: 2),
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 20,
          ),
          ValueListenableBuilder(
            valueListenable: showValue,
            builder: (_, bool value, Widget? child) {
              if (value) {
                return Text('A média é: ${valueToShow.toStringAsFixed(2)}');
              }
              return Container();
            },
          )
        ],
      ),
    );
  }

  onChanged(String? value) {
    if (!_formKey.currentState!.validate()) return;
    setState(() {
      showValue.value = true;
      valueToShow = (double.parse(_controller1.text.trim()) +
              double.parse(_controller2.text.trim()) +
              double.parse(_controller3.text.trim())) /
          3;
    });
  }

  String? validator(String? value) {
    if (value!.isEmpty) {
      showValue.value = false;
      return 'Digite um número.';
    }

    return null;
  }
}
