import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdm22/core/widgets/custom_text_form_field.dart';
import 'package:pdm22/core/widgets/decimal_input_formatter.dart';

class Exer12Body extends StatefulWidget {
  const Exer12Body({Key? key}) : super(key: key);

  @override
  State<Exer12Body> createState() => _Exer12BodyState();
}

class _Exer12BodyState extends State<Exer12Body> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _controller = TextEditingController();

  ValueNotifier<bool> showValue = ValueNotifier<bool>(false);

  late String valueToShow;

  @override
  void initState() {
    super.initState();
    _formKey.currentState?.reset();
    showValue.value = false;
    _controller.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          CustomTextFormField(
            label: 'Valor',
            hint: 'Digite um valor positivo',
            controller: _controller,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.number,
            inputFormatters: [
              DecimalTextInputFormatter(decimalRange: 2),
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 20,
          ),
          ValueListenableBuilder(
            valueListenable: showValue,
            builder: (_, bool value, Widget? child) {
              if (value) {
                return Text(valueToShow);
              }
              return Container();
            },
          )
        ],
      ),
    );
  }

  onChanged(String? value) {
    if (!_formKey.currentState!.validate()) return;
    setState(() {
      showValue.value = true;
      valueToShow = results();
    });
  }

  String results() {
    double atual = double.parse(_controller.text.trim());
    double ano1Reajuste = atual * 0.07;
    double ano2Reajuste = atual * 0.06 + ano1Reajuste;
    double ano3Reajuste = atual * 0.05 + ano2Reajuste;
    return 'Atual: R\$ ${atual.toStringAsFixed(2)}\nReajuste ano 1: R\$ ${(atual + ano1Reajuste).toStringAsFixed(2)}\nReajuste ano 2: R\$ ${(atual + ano2Reajuste).toStringAsFixed(2)}\nReajuste ano 3: R\$ ${(atual + ano3Reajuste).toStringAsFixed(2)}';
  }

  String? validator(String? value) {
    if (value!.isEmpty) {
      showValue.value = false;
      return 'Digite um número positivo.';
    }

    return null;
  }
}
