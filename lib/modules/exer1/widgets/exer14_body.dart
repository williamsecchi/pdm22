import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdm22/core/widgets/custom_text_form_field.dart';

class Exer14Body extends StatefulWidget {
  const Exer14Body({Key? key}) : super(key: key);

  @override
  State<Exer14Body> createState() => _Exer14BodyState();
}

class _Exer14BodyState extends State<Exer14Body> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _controllerC1 = TextEditingController();

  final TextEditingController _controllerC2 = TextEditingController();

  ValueNotifier<bool> showValue = ValueNotifier<bool>(false);

  late double valueToShow;

  @override
  void initState() {
    super.initState();
    _formKey.currentState?.reset();
    showValue.value = false;
    _controllerC1.clear();
    _controllerC2.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          CustomTextFormField(
            label: 'Cateto 1',
            hint: 'Digite um valor',
            controller: _controllerC1,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 10,
          ),
          CustomTextFormField(
            label: 'Cateto 2',
            hint: 'Digite um valor',
            controller: _controllerC2,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 20,
          ),
          ValueListenableBuilder(
            valueListenable: showValue,
            builder: (_, bool value, Widget? child) {
              if (value) {
                return Text('A hipotenusa mede: ${valueToShow.toStringAsFixed(2)}');
              }
              return Container();
            },
          )
        ],
      ),
    );
  }

  onChanged(String? value) {
    if (!_formKey.currentState!.validate()) return;
    setState(() {
      showValue.value = true;
      valueToShow = result();
    });
  }

  double result() {
    double c1 = double.parse(_controllerC1.text.trim());
    double c2 = double.parse(_controllerC2.text.trim());

    num h = pow(c1, 2) + pow(c2, 2);

    double value = h.toDouble();

    return sqrt(value);
  }

  String? validator(String? value) {
    if (value!.isEmpty) {
      showValue.value = false;
      return 'Digite um número positivo.';
    }

    return null;
  }
}
