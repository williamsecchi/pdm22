import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:pdm22/core/widgets/custom_text_form_field.dart';

class Exer5Body extends StatefulWidget {
  const Exer5Body({Key? key}) : super(key: key);

  @override
  State<Exer5Body> createState() => _Exer5BodyState();
}

class _Exer5BodyState extends State<Exer5Body> {
  final DateTime date = DateTime.now();

  final DateFormat formatter = DateFormat('dd/MM/yyyy');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text('Dia: ${date.day.toString()}'),
        Text('Mês: ${date.month.toString()}'),
        Text('Ano: ${date.year.toString()}'),
        Text('${date.day.toString()}/${date.month.toString()}/${date.year.toString()}'),
        Text(formatter.format(date)),
      ],
    );
  }
}
