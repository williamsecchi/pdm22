import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdm22/core/widgets/custom_text_form_field.dart';
import 'dart:math';

class Exer6Body extends StatefulWidget {
  const Exer6Body({Key? key}) : super(key: key);

  @override
  State<Exer6Body> createState() => _Exer6BodyState();
}

class _Exer6BodyState extends State<Exer6Body> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _controllerA = TextEditingController();

  final TextEditingController _controllerR = TextEditingController();

  ValueNotifier<bool> showValue = ValueNotifier<bool>(false);

  late double valueToShow;

  @override
  void initState() {
    super.initState();
    _formKey.currentState?.reset();
    showValue.value = false;
    _controllerA.clear();
    _controllerR.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          CustomTextFormField(
            label: 'Altura',
            hint: 'Digite um valor para a altura',
            controller: _controllerA,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          CustomTextFormField(
            label: 'Raio',
            hint: 'Digite um valor para o raio',
            controller: _controllerR,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: () {
              if (!_formKey.currentState!.validate()) return;
              int r = int.parse(_controllerR.text.trim());
              int a = int.parse(_controllerA.text.trim());
              int auxR = 0;
              for (int i = 0; i < r; i++) {
                auxR += r;
              }
              double v = pi * auxR * a;

              setState(
                () {
                  showValue.value = true;
                  valueToShow = v;
                },
              );
            },
            child: Container(
              margin: const EdgeInsets.all(16.0),
              width: 200,
              height: 50,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15), color: Colors.blue),
              child: const Text(
                'Calcular',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
          ValueListenableBuilder(
            valueListenable: showValue,
            builder: (_, bool value, Widget? child) {
              if (value) {
                return Text('O volume é: ${valueToShow.toStringAsFixed(2)}');
              }
              return Container();
            },
          )
        ],
      ),
    );
  }

  String? validator(String? value) {
    if (value!.isEmpty) {
      return 'Digite um valor.';
    }

    return null;
  }
}
