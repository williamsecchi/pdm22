import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdm22/core/widgets/custom_text_form_field.dart';

class Exer9Body extends StatefulWidget {
  const Exer9Body({Key? key}) : super(key: key);

  @override
  State<Exer9Body> createState() => _Exer9BodyState();
}

class _Exer9BodyState extends State<Exer9Body> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _controllerH = TextEditingController();

  final TextEditingController _controllerM = TextEditingController();

  final TextEditingController _controllerS = TextEditingController();

  ValueNotifier<bool> showValue = ValueNotifier<bool>(false);

  late int valueToShow;

  @override
  void initState() {
    super.initState();
    _formKey.currentState?.reset();
    showValue.value = false;
    _controllerH.clear();
    _controllerM.clear();
    _controllerS.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          CustomTextFormField(
            label: 'Horas',
            hint: 'Digite um valor',
            controller: _controllerH,
            validator: validator,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 10,
          ),
          CustomTextFormField(
            label: 'Minutos',
            hint: 'Digite um valor',
            controller: _controllerM,
            validator: validator,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 10,
          ),
          CustomTextFormField(
            label: 'Segundos',
            hint: 'Digite um valor',
            controller: _controllerS,
            validator: validator,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 20,
          ),
          ValueListenableBuilder(
            valueListenable: showValue,
            builder: (_, bool value, Widget? child) {
              if (value) {
                return Text('Total: $valueToShow segundos');
              }
              return Container();
            },
          )
        ],
      ),
    );
  }

  onChanged(String? value) {
    if (!_formKey.currentState!.validate()) return;
    setState(() {
      showValue.value = true;
      valueToShow = operation();
    });
  }

  int operation() {
    int h = int.parse(_controllerH.text.trim()) * 60 * 60;
    int m = int.parse(_controllerM.text.trim()) * 60;
    int s = int.parse(_controllerS.text.trim());
    return h + m + s;
  }

  String? validator(String? value) {
    if (value!.isEmpty) {
      showValue.value = false;
      return 'Digite um número positivo.';
    }

    return null;
  }
}
