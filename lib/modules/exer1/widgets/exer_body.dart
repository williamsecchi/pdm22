import 'package:flutter/material.dart';

import '../models/questions.dart';

class ExerBody extends StatefulWidget {
  ExerBody({
    Key? key,
    required this.questions,
    required this.child,
  }) : super(key: key);

  final Questions questions;
  Widget child;

  @override
  State<ExerBody> createState() => _ExerBodyState();
}

class _ExerBodyState extends State<ExerBody> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  AppBar _appBar() {
    return AppBar(
      title: Text(widget.questions.title ?? ''),
    );
  }

  SingleChildScrollView _body() {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Text(widget.questions.question ?? ''),
            if (widget.questions.topics != null)
              ListView.builder(
                shrinkWrap: true,
                itemCount: widget.questions.topics?.length,
                itemBuilder: (context, index) =>
                    Text('${index + 1}. ${widget.questions.topics?[index].topic ?? ''}'),
              ),
            const SizedBox(
              height: 30,
            ),
            widget.child,
          ],
        ),
      ),
    );
  }
}
