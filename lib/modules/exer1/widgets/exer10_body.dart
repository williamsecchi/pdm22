import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdm22/core/widgets/custom_text_form_field.dart';

class Exer10Body extends StatefulWidget {
  const Exer10Body({Key? key}) : super(key: key);

  @override
  State<Exer10Body> createState() => _Exer10BodyState();
}

class _Exer10BodyState extends State<Exer10Body> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _controller = TextEditingController();

  ValueNotifier<bool> showValue = ValueNotifier<bool>(false);

  late String valueToShow;

  @override
  void initState() {
    super.initState();
    _formKey.currentState?.reset();
    showValue.value = false;
    _controller.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          CustomTextFormField(
            label: 'Valor',
            hint: 'Digite o total de dias',
            controller: _controller,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 20,
          ),
          ValueListenableBuilder(
            valueListenable: showValue,
            builder: (_, bool value, Widget? child) {
              if (value) {
                return Text(valueToShow);
              }
              return Container();
            },
          )
        ],
      ),
    );
  }

  onChanged(String? value) {
    if (!_formKey.currentState!.validate()) return;
    setState(() {
      showValue.value = true;
      valueToShow = results();
    });
  }

  String results() {
    int total = int.parse(_controller.text.trim()) * 20;
    double percImposto = total * 0.08;
    double liquido = total - percImposto;
    return 'Total: R\$ ${total.toStringAsFixed(2)}\nIR: R\$ ${percImposto.toStringAsFixed(2)}\nLiquido: R\$ ${liquido.toStringAsFixed(2)}';
  }

  String? validator(String? value) {
    if (value!.isEmpty) {
      showValue.value = false;
      return 'Digite um número positivo.';
    }

    return null;
  }
}
