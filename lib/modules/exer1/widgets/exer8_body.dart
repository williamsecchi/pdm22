import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdm22/core/widgets/custom_text_form_field.dart';

class Exer8Body extends StatefulWidget {
  const Exer8Body({Key? key}) : super(key: key);

  @override
  State<Exer8Body> createState() => _Exer8BodyState();
}

class _Exer8BodyState extends State<Exer8Body> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _controllerA = TextEditingController();

  final TextEditingController _controllerB = TextEditingController();

  final TextEditingController _controllerC = TextEditingController();

  ValueNotifier<bool> showValue = ValueNotifier<bool>(false);

  late String valueToShow;

  @override
  void initState() {
    super.initState();
    _formKey.currentState?.reset();
    showValue.value = false;
    _controllerA.clear();
    _controllerB.clear();
    _controllerC.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          CustomTextFormField(
            label: 'Valor A',
            hint: 'Digite um valor',
            controller: _controllerA,
            validator: validator,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 10,
          ),
          CustomTextFormField(
            label: 'Valor B',
            hint: 'Digite um valor',
            controller: _controllerB,
            validator: validator,
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 10,
          ),
          CustomTextFormField(
            label: 'Valor C',
            hint: 'Digite um valor',
            controller: _controllerC,
            validator: validator,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
            ],
            onChanged: onChanged,
          ),
          const SizedBox(
            height: 20,
          ),
          ValueListenableBuilder(
            valueListenable: showValue,
            builder: (_, bool value, Widget? child) {
              if (value) {
                return Text(valueToShow);
              }
              return Container();
            },
          )
        ],
      ),
    );
  }

  onChanged(String? value) {
    if (!_formKey.currentState!.validate()) return;

    String auxText = '';
    auxText += triangulo();
    auxText += circulo();
    auxText += trapezio();
    auxText += quadrado();
    auxText += retangulo();

    setState(() {
      showValue.value = true;
      valueToShow = auxText;
    });
  }

  String triangulo() {
    return '1 = ${((int.parse(_controllerA.text.trim()) * int.parse(_controllerB.text.trim())) / 2).toStringAsFixed(2)}\n';
  }

  String circulo() {
    num auxR = pow(int.parse(_controllerC.text.trim()), 2);
    return '2 = ${(pi * auxR.toInt()).toStringAsFixed(2)}\n';
  }

  String trapezio() {
    return '3 = ${(((int.parse(_controllerA.text.trim()) + int.parse(_controllerB.text.trim())) * int.parse(_controllerC.text.trim())) / 2).toStringAsFixed(2)}\n';
  }

  String quadrado() {
    return '4 = ${pow(int.parse(_controllerB.text.trim()), 2)}\n';
  }

  String retangulo() {
    return '5 = ${int.parse(_controllerA.text.trim()) * int.parse(_controllerC.text.trim())}';
  }

  String? validator(String? value) {
    if (value!.isEmpty) {
      showValue.value = false;
      return 'Digite um número.';
    }

    return null;
  }
}
