
import '../models/exer1_screen_model.dart';
import '../models/questions.dart';

class Exer1ScreenViewModel {
    final Exer1ScreenModel homeScreenModel = Exer1ScreenModel();

    List<Questions> get questions => homeScreenModel.questions;
}
