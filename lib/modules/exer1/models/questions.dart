class Questions {
  int? id;
  String? question;
  String? route;
  String? title;
  String? subtitle;
  List<Topics>? topics;

  Questions({
    this.id,
    this.question,
    this.route,
    this.title,
    this.subtitle,
    this.topics,
  });

  @override
  String toString() {
    return 'Questions{id: $id, question: $question, route: $route, title: $title, subtitle: $subtitle, topics: $topics}';
  }
}

class Topics {
  int? id;
  String? topic;

  Topics({
    this.id,
    this.topic,
  });

  @override
  String toString() {
    return 'Topics{id: $id, topic: $topic}';
  }
}
