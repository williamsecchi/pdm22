
import 'package:pdm22/modules/exer1/models/questions.dart';

class Exer1ScreenModel {
  List<Questions> questions = <Questions>[
    Questions(
      id: 1,
      route: '/',
      title: 'Exercício 1',
      subtitle: 'Escrever um programa que lê um determinado',
      question:
          'Escrever um programa que lê um determinado valor positivo e calcula seu dobro.',
    ),
    Questions(
      id: 2,
      route: '/',
      title: 'Exercício 2',
      subtitle: 'Escreva um programa que leia dois números',
      question:
          'Escreva um programa que leia dois números em ponto flutuante e imprima a soma desses números.',
    ),
    Questions(
      id: 3,
      route: '/',
      title: 'Exercício 3',
      subtitle: 'Escreva um programa que leia um inteiro',
      question:
          'Escreva um programa que leia um inteiro, calcule e imprima o quadrado no número lido.',
    ),
    Questions(
      id: 4,
      route: '/',
      title: 'Exercício 4',
      subtitle: 'Escreva um programa que leia três números',
      question:
          'Escreva um programa que leia três números em ponto flutuante e imprima a média aritmética entre eles.',
    ),
    Questions(
      id: 5,
      route: '/',
      title: 'Exercício 5',
      subtitle: 'Escreva um programa que inicialize uma variável',
      question:
          'Escreva um programa que inicialize uma variável com o dia de hoje, outra com o mês e outra com o ano e imprima a data na tela no formato “dd/mm/aaaa”.',
    ),
    Questions(
      id: 6,
      route: '/',
      title: 'Exercício 6',
      subtitle: 'Escreva um programa que leia a altura e o raio',
      question:
          'Escreva um programa que leia a altura e o raio de um cilindro circular e imprima o volume do mesmo, segundo a fórmula: V = pi.raio².altura ->obs: Imprima o volume com uma precisão de duas casas decimais.',
    ),
    Questions(
      id: 7,
      route: '/',
      title: 'Exercício 7',
      subtitle: 'Faca um programa que converta uma medida de temperatura ',
      question:
          'Faca um programa que converta uma medida de temperatura de Fahrenheit para Celsius. A partir da fórmula de conversão de Celsius para Fahrenheit, que é C = (5/9 (F-32)) , deduza a fórmula para a conversão de Fahrenheit para Celsius para que você possa resolver o problema.',
    ),
    Questions(
      id: 8,
      route: '/',
      title: 'Exercício 8',
      subtitle: 'Escrever um programa que lê 3 valores',
      question:
          'Escrever um programa que lê 3 valores reais a, b e c e calcula:',
      topics: <Topics>[
        Topics(
          id: 1,
          topic: 'a área do triângulo que tem a por base e b por altura.',
        ),
        Topics(
          id: 2,
          topic: 'a área do círculo de raio c.',
        ),
        Topics(
          id: 3,
          topic: 'a área do trapézio que tem a e b por bases e c por altura.',
        ),
        Topics(
          id: 4,
          topic: 'a área do quadrado de lado b.',
        ),
        Topics(
          id: 5,
          topic: 'a área do retângulo de lados a e b.',
        ),
      ],
    ),
    Questions(
        id: 9,
        route: '/',
        title: 'Exercício 9',
        subtitle: 'Escreva um programa leia uma quantidade',
        question:
            'Escreva um programa leia uma quantidade de tempo dada em horas, minutos e segundos e converta para um número equivalente em segundos.'),
    Questions(
      id: 10,
      route: '/',
      title: 'Exercício 10',
      subtitle: 'Uma firma contrata um encanador a R\$ 20,00 por dia',
      question:
          'Uma firma contrata um encanador a R\$ 20,00 por dia. Escreva um programa que leia o número de dias trabalhados pelo encanador e imprima a quantia líquida que deverá ser paga, sabendo-se que são descontados 8% para o imposto de renda.',
    ),
    Questions(
      id: 11,
      route: '/',
      title: 'Exercício 11',
      subtitle: 'Uma companhia telefônica opera com a seguinte tarifa',
      question:
          'Uma companhia telefônica opera com a seguinte tarifa: uma chamada telefônica com duração de 3 minutos custa R\$ 1,15. Cada minuto adicional custa R\$ 0,26. Escreva um programa que leia a duração total de uma chamada (em minutos) e calcule o total a ser pago.',
    ),
    Questions(
      id: 12,
      route: '/',
      title: 'Exercício 12',
      subtitle: 'Em uma empresa os funcionários renovam o contrato',
      question:
          'Em uma empresa os funcionários renovam o contrato por três anos. O salário sofrerá um reajuste de 7%, 6% e 5%, respectivamente, nos próximos três anos. Escreva um programa que leia o salário mensal atual do funcionário, e então, imprima o salário mensal para cada um dos três próximos anos.',
    ),
    Questions(
      id: 13,
      route: '/',
      title: 'Exercício 13',
      subtitle: 'Faça um programa que peça ao usuário',
      question:
          'Faça um programa que peça ao usuário a quilometragem atual, a quilometragem anterior, a quantidade de litros consumida e informe a taxa de consumo (quilômetros por hora) de um automóvel.',
    ),
    Questions(
      id: 14,
      route: '/',
      title: 'Exercício 14',
      subtitle: 'Faça um programa que peça ao usuário',
      question:
          'Faça um programa que peça ao usuário os tamanhos dos catetos de um triângulo retângulo e mostre na tela o valor de sua hipotenusa.',
    ),
  ];
}
