import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:modular_interfaces/modular_interfaces.dart';
import 'package:pdm22/modules/exer1/widgets/exer10_body.dart';
import 'package:pdm22/modules/exer1/widgets/exer12_body.dart';
import 'package:pdm22/modules/exer1/widgets/exer13_body.dart';
import 'package:pdm22/modules/exer1/widgets/exer14_body.dart';


import 'models/questions.dart';
import 'view/exer1_screen.dart';
import 'viewmodel/exer1_screen_viewmodel.dart';
import 'widgets/exer11_body.dart';
import 'widgets/exer1_body.dart';
import 'widgets/exer2_body.dart';
import 'widgets/exer3_body.dart';
import 'widgets/exer4_body.dart';
import 'widgets/exer5_body.dart';
import 'widgets/exer6_body.dart';
import 'widgets/exer7_body.dart';
import 'widgets/exer8_body.dart';
import 'widgets/exer9_body.dart';
import 'widgets/exer_body.dart';

class Exer1Module extends Module {
  @override
  List<Bind> get binds => <Bind<Object>>[
        Bind<Exer1ScreenViewModel>(
          (Injector<dynamic> i) => Exer1ScreenViewModel(),
        ),
      ];

  @override
  List<ModularRoute> get routes => <ModularRoute>[
        ChildRoute(
          '/',
          child: (context, args) {
            return const Exer1Screen();
          },
        ),
        ChildRoute(
          '/operation',
          child: (BuildContext context, ModularArguments args) {
            Questions? question = args.data as Questions;
            switch (question.id) {
              case 1:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer1Body(),
                  );
                }
              case 2:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer2Body(),
                  );
                }
              case 3:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer3Body(),
                  );
                }
              case 4:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer4Body(),
                  );
                }
              case 5:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer5Body(),
                  );
                }
              case 6:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer6Body(),
                  );
                }
              case 7:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer7Body(),
                  );
                }
              case 8:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer8Body(),
                  );
                }
              case 9:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer9Body(),
                  );
                }
              case 10:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer10Body(),
                  );
                }
              case 11:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer11Body(),
                  );
                }
              case 12:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer12Body(),
                  );
                }
              case 13:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer13Body(),
                  );
                }
              case 14:
                {
                  return ExerBody(
                    questions: question,
                    child: const Exer14Body(),
                  );
                }
            }

            return const Exer1Screen();
          },
          transition: TransitionType.rightToLeft,
        )
      ];
}
