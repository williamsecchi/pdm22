import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../viewmodel/exer1_screen_viewmodel.dart';

class Exer1Screen extends StatefulWidget {
  const Exer1Screen({Key? key}) : super(key: key);

  @override
  State<Exer1Screen> createState() => _Exer1ScreenState();
}

class _Exer1ScreenState extends State<Exer1Screen> {
  final Exer1ScreenViewModel instance = Modular.get<Exer1ScreenViewModel>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(),
      body: _body(),
    );
  }

  AppBar _appBar() {
    return AppBar(
      title: const Text('Exer 1'),
    );
  }

  Container _body() {
    return Container(
      padding: const EdgeInsets.all(16.0),
      child: ListView.builder(
        itemCount: instance.questions.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(instance.questions[index].title ?? ''),
            subtitle: Text(instance.questions[index].subtitle ?? ''),
            onTap: () {
              Navigator.pushNamed(
                context,
                '${Modular.to.path}operation',
                arguments: instance.questions[index],
              );
            },
          );
        },
      ),
    );
  }
}
