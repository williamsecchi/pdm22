import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pdm22/modules/splash_module/viewmodel/splash_screen_viewmodel.dart';
import 'package:modular_interfaces/modular_interfaces.dart';

import 'view/splash_screen.dart';

class SplashModule extends Module {
  @override
  List<Bind> get binds => <Bind<Object>>[
        Bind<SplashScreenViewModel>(
          (Injector<dynamic> i) => SplashScreenViewModel(),
        ),
      ];

  @override
  List<ModularRoute> get routes => <ModularRoute>[
        ChildRoute(
          '/',
          child: (BuildContext context, args) {
            return const SplashScreen();
          },
        ),
      ];
}
