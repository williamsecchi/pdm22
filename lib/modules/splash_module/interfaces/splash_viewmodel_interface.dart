abstract class ISplashViewModelInterface {
  void navigate(String screenName);
}