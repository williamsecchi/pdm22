import 'package:pdm22/modules/splash_module/interfaces/splash_viewmodel_interface.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SplashScreenViewModel implements ISplashViewModelInterface {
  @override
  void navigate(String screenName) {
    Modular.to.navigate('${Modular.to.path}$screenName/');
  }
}
