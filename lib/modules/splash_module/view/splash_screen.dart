import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:pdm22/modules/splash_module/viewmodel/splash_screen_viewmodel.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
    );
  }

  Container _body() {
    return Container(
      color: Colors.white,
      child: Center(
        child: AnimatedTextKit(
          isRepeatingAnimation: false,
          onFinished: () =>
              Modular.get<SplashScreenViewModel>().navigate('home'),
          animatedTexts: [
            TyperAnimatedText(
              'Seja bem vindo(a)\nprojeto TDE disciplina de PDM 2022',
              speed: const Duration(milliseconds: 100),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
